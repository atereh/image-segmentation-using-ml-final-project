**STATEMENTING BUILDINGS IN IMAGES**

**AUTOMATIC PIXEL WISE LABELING OF AERIAL IMAGERY**


Inria problem and dataset - https://project.inria.fr/aerialimagelabeling/


We solved the problem of image segmentation of Geo images using traditional machine learning algorithms, 
such as **SVM, Decision tree and Random Forest**. 

We already used **LBP, Haralick texture and spectral features** for training models. Check the report and code comments for more information.

### Contributors
- Alina Terekhova

- Fatemeh Changizian